// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at https://mozilla.org/MPL/2.0/.

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

type shopOutput struct {
	Name  string `json:"name"`
	Hours string `json:"hours"`
}

func main() {
	port := os.Getenv("DASHBOARD_PORT")
	if port == "" {
		port = "8080"
	}
	notionApiKey := os.Getenv("DASHBOARD_NOTION_API_KEY")
	notionShopHoursDB := os.Getenv("DASHBOARD_NOTION_SHOP_HOURS_DB")

	mux := &http.ServeMux{}
	server := &http.Server{
		Addr:         fmt.Sprintf("127.0.0.1:%s", port),
		Handler:      mux,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
	}

	mux.HandleFunc("/frontend/time.html", func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		zoom, _ := strconv.Atoi(r.Form.Get("zoom"))
		if zoom == 0 {
			zoom = 10
		}

		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "text/html")
		_, err = w.Write([]byte(fmt.Sprintf("<body style=\"background-color: rgba(0, 0, 0, 0); margin: 0\"><div id=\"time\" style=\"color: #DDD; font-size: %dpt; font-family: sans-serif; text-align: center\"></div>\n<script>\nlet timeElem = document.getElementById(\"time\")\nsetInterval(() => {fetch(\"/api/time.txt\").then((response) => {return response.text()}).then((time) => {timeElem.innerHTML = time})}, 1000)\n</script></body>", zoom)))
		if err != nil {
			log.Printf("error writing to output: %s", err)
		}
	})

	mux.HandleFunc("/frontend/weather.html", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "text/html")
		_, err := w.Write([]byte("<html lang=\"sk\"><head><title></title></head><body style=\"background-color: rgba(0, 0, 0, 0); margin: 0\"><iframe id=\"weather\" src=\"https://www.meteoblue.com/sk/po%C4%8Dasie/widget/three/bratislava_slovensk%c3%a1-republika_3060972?geoloc=fixed&nocurrent=0&noforecast=0&days=4&tempunit=CELSIUS&windunit=KILOMETER_PER_HOUR&layout=image\"  frameborder=\"0\" scrolling=\"NO\" allowtransparency=\"true\" sandbox=\"allow-same-origin allow-scripts allow-popups allow-popups-to-escape-sandbox\" style=\"width: 100%; height: 100%\"></iframe><div><!-- DO NOT REMOVE THIS LINK --><a href=\"https://www.meteoblue.com/sk/po%C4%8Dasie/t%C3%BD%C5%BEde%C5%88/bratislava_slovensk%c3%a1-republika_3060972?utm_source=weather_widget&utm_medium=linkus&utm_content=three&utm_campaign=Weather%2BWidget\" target=\"_blank\" rel=\"noopener\">meteoblue</a></div><script>let weatherFrame = document.getElementById(\"weather\");setInterval(() => {let timestampMinutes = Math.floor(Date.now() / 1000 / 60), threeHoursMinutes = 60 * 3; if (timestampMinutes % threeHoursMinutes === 0) {weatherFrame.src = weatherFrame.src}}, 1000 * 60)</script></body></html>"))
		if err != nil {
			log.Printf("error writing to output: %s", err)
		}
	})

	mux.HandleFunc("/frontend/shops.html", func(w http.ResponseWriter, r *http.Request) {
		err := r.ParseForm()
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		zoom, _ := strconv.Atoi(r.Form.Get("zoom"))
		if zoom == 0 {
			zoom = 10
		}

		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "text/html")
		_, err = w.Write([]byte(fmt.Sprintf("<html lang=\"sk\"><head><title></title><style>td{padding:5px 10px}</style></head><body style=\"background-color: rgba(0, 0, 0, 0); margin: 0\"><table style=\"color: #DDD; font-size: %dpt; font-family: sans-serif\"><tbody id=\"shops\"></tbody></table>\n<script>\nlet shopsElem = document.getElementById(\"shops\"), fetchShopData = function () {fetch(\"/api/shops.json\").then((response) => {return response.json()}).then((shops) => {shops = shops.map((shop) => {let shopElem = document.createElement(\"tr\"), nameElem = document.createElement(\"td\"), hoursElem = document.createElement(\"td\");nameElem.innerHTML = shop.name;hoursElem.innerHTML = shop.hours;shopElem.append(nameElem, hoursElem);return shopElem});shopsElem.replaceChildren(...shops)})};fetchShopData();setInterval(() => fetchShopData(), 1000 * 60)\n</script></body></html>", zoom)))
		if err != nil {
			log.Printf("error writing to output: %s", err)
		}
	})

	mux.HandleFunc("/api/time.txt", func(w http.ResponseWriter, r *http.Request) {
		now := time.Now()
		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "text/plain")
		_, err := w.Write([]byte(now.Format("15:04:05")))
		if err != nil {
			log.Printf("error writing to output: %s", err)
		}
	})

	mux.HandleFunc("/api/shops.json", func(w http.ResponseWriter, r *http.Request) {
		now := time.Now()
		shops, err := getShopHours(notionApiKey, notionShopHoursDB)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, err2 := w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
			if err2 != nil {
				log.Printf("error writing to output: %s", err2)
			}
		}

		output := make([]shopOutput, 0, len(shops))
		for _, shop := range shops {
			open, hoursToChange, minutesToChange := getOpenAndTimeToChange(now, shop.dailyHours)

			var openText, timeText string
			if open {
				openText = "zatvára"
			} else {
				openText = "otvára"
			}
			if hoursToChange > 0 {
				timeText = fmt.Sprintf("%dh %dm", hoursToChange, minutesToChange)
			} else {
				timeText = fmt.Sprintf("%dm", minutesToChange)
			}

			output = append(output, shopOutput{
				Name:  shop.shopName,
				Hours: fmt.Sprintf("%s o %s", openText, timeText)})
		}
		outputRaw, err := json.Marshal(output)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			_, err2 := w.Write([]byte(http.StatusText(http.StatusInternalServerError)))
			if err2 != nil {
				log.Printf("error writing to output: %s", err2)
			}
		}

		w.WriteHeader(http.StatusOK)
		w.Header().Set("Content-Type", "application/json")
		_, err = w.Write(outputRaw)
		if err != nil {
			log.Printf("error writing to output: %s", err)
		}
	})

	log.Fatal(server.ListenAndServe())
}

func getOpenAndTimeToChange(now time.Time, hours map[uint8]openingHours) (bool, uint8, uint8) {
	todayHours := hours[uint8(now.Weekday())]
	opens := time.Date(
		now.Year(),
		now.Month(),
		now.Day(),
		int(todayHours.openHour), // we do not need to check for -1 here, time package will handle it okay
		int(todayHours.openMinute),
		0,
		0,
		time.Local,
	)
	closes := time.Date(
		now.Year(),
		now.Month(),
		now.Day(),
		int(todayHours.closeHour),
		int(todayHours.closeMinute),
		0,
		0,
		time.Local,
	)

	var open bool
	var timeToChange time.Duration
	if (now.After(opens) || now.Equal(opens)) && now.Before(closes) {
		open = true
		timeToChange = closes.Sub(now)
	} else {
		open = false
		if now.After(opens) {
			for i := 1; i <= 7; i++ {
				dayHours := hours[uint8((int(now.Weekday())+i)%7)]
				if dayHours.openHour != -1 {
					opens = time.Date(
						now.Year(),
						now.Month(),
						now.Day()+i, // will be normalised by time package
						int(dayHours.openHour),
						int(dayHours.openMinute),
						0,
						0,
						time.Local,
					)
					break
				}
			}
		}
		timeToChange = opens.Sub(now)
	}

	hoursToChange := timeToChange.Hours()
	timeToChange -= timeToChange.Truncate(time.Hour)
	minutesToChange := timeToChange.Truncate(time.Minute).Minutes()

	return open, uint8(hoursToChange), uint8(minutesToChange)
}
