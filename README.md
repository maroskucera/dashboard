Dashboard
=========

Dashboard is a simple app that serves as a source of embeddable blocks I use
on my home Dashboard. It has three frontend views and two API endpoints.
Frontend views handle displaying of HTML and periodically reloading the data
and the API endpoints serve as sources of the data.

Provided HTML views are:

- current server time (size controllable by query param `zoom`)
- weather in Bratislava, Slovakia (embed from
  [Meteoblue](https://www.meteoblue.com))
- opening hours of shops sourced from [Notion](https://www.notion.so) database 
  with a certain structure (size controllable by query param `zoom`)

API endpoints provide:

- current server time as a text
- relative opening hours (in Slovak language) sourced from Notion database as
  a JSON

The desired structure of Notion database can be derived from `notionResponse`
struct in `notionclient.go` file. In short, it has a set of opening hours and
minutes and closing hours and minutes (as numbers).

There are three environment variables used by Dashboard, they are as follows:

- `DASHBOARD_PORT` is the port on which the views and endpoints are served (I
  strongly recommend putting it behind a reverse proxy)
- `DASHBOARD_NOTION_API_KEY` is the API key used for authenticating with Notion
  API
- `DASHBOARD_NOTION_SHOP_HOURS_DB` is the ID of the database that holds the shop
  opening hours
  ([this page](https://developers.notion.com/docs/working-with-databases) should
  provide information on how to determine the database ID in your workspace)

Licence
-------

Dashboard is licensed under the
[Mozilla Public License, v. 2.0](https://mozilla.org/MPL/2.0/).

Status
------

Dashboard is intended for my personal use only. I published it in hope that it
may be useful or educational for someone else. I do not intend to maintain it
or to make significant changes to it. If you need adjustments made, please copy
the code and do them yourself.

Author
------

Maros Kucera <maros (at) maroskucera (dot) com>