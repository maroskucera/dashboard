// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at https://mozilla.org/MPL/2.0/.

package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

type openingHours struct {
	openHour    int8
	openMinute  int8
	closeHour   int8
	closeMinute int8
}

type shopHours struct {
	shopName   string
	dailyHours map[uint8]openingHours
}

type notionResponse struct {
	Object  string `json:"object"`
	Results []struct {
		Object     string `json:"object"`
		Id         string `json:"id"`
		Properties struct {
			Name struct {
				Title []struct {
					Text struct {
						Content string `json:"content"`
					} `json:"text"`
				} `json:"title"`
			} `json:"Name"`
			MondayOpenHr struct {
				Number int8 `json:"number"`
			} `json:"Monday Opening Hr."`
			MondayOpenMin struct {
				Number int8 `json:"number"`
			} `json:"Monday Opening Min."`
			MondayCloseHr struct {
				Number int8 `json:"number"`
			} `json:"Monday Closing Hr."`
			MondayCloseMin struct {
				Number int8 `json:"number"`
			} `json:"Monday Closing Min."`
			TuesdayOpenHr struct {
				Number int8 `json:"number"`
			} `json:"Tuesday Opening Hr."`
			TuesdayOpenMin struct {
				Number int8 `json:"number"`
			} `json:"Tuesday Opening Min."`
			TuesdayCloseHr struct {
				Number int8 `json:"number"`
			} `json:"Tuesday Closing Hr."`
			TuesdayCloseMin struct {
				Number int8 `json:"number"`
			} `json:"Tuesday Closing Min."`
			WednesdayOpenHr struct {
				Number int8 `json:"number"`
			} `json:"Wednesday Opening Hr."`
			WednesdayOpenMin struct {
				Number int8 `json:"number"`
			} `json:"Wednesday Opening Min."`
			WednesdayCloseHr struct {
				Number int8 `json:"number"`
			} `json:"Wednesday Closing Hr."`
			WednesdayCloseMin struct {
				Number int8 `json:"number"`
			} `json:"Wednesday Closing Min."`
			ThursdayOpenHr struct {
				Number int8 `json:"number"`
			} `json:"Thursday Opening Hr."`
			ThursdayOpenMin struct {
				Number int8 `json:"number"`
			} `json:"Thursday Opening Min."`
			ThursdayCloseHr struct {
				Number int8 `json:"number"`
			} `json:"Thursday Closing Hr."`
			ThursdayCloseMin struct {
				Number int8 `json:"number"`
			} `json:"Thursday Closing Min."`
			FridayOpenHr struct {
				Number int8 `json:"number"`
			} `json:"Friday Opening Hr."`
			FridayOpenMin struct {
				Number int8 `json:"number"`
			} `json:"Friday Opening Min."`
			FridayCloseHr struct {
				Number int8 `json:"number"`
			} `json:"Friday Closing Hr."`
			FridayCloseMin struct {
				Number int8 `json:"number"`
			} `json:"Friday Closing Min."`
			SaturdayOpenHr struct {
				Number int8 `json:"number"`
			} `json:"Saturday Opening Hr."`
			SaturdayOpenMin struct {
				Number int8 `json:"number"`
			} `json:"Saturday Opening Min."`
			SaturdayCloseHr struct {
				Number int8 `json:"number"`
			} `json:"Saturday Closing Hr."`
			SaturdayCloseMin struct {
				Number int8 `json:"number"`
			} `json:"Saturday Closing Min."`
			SundayOpenHr struct {
				Number int8 `json:"number"`
			} `json:"Sunday Opening Hr."`
			SundayOpenMin struct {
				Number int8 `json:"number"`
			} `json:"Sunday Opening Min."`
			SundayCloseHr struct {
				Number int8 `json:"number"`
			} `json:"Sunday Closing Hr."`
			SundayCloseMin struct {
				Number int8 `json:"number"`
			} `json:"Sunday Closing Min."`
		} `json:"properties"`
		Url string `json:"url"`
	} `json:"results"`
}

func getShopHours(apiKey string, database string) ([]shopHours, error) {
	client := http.Client{
		Timeout: 5 * time.Second,
	}

	request, err := http.NewRequest(
		"POST",
		fmt.Sprintf("https://api.notion.com/v1/databases/%s/query", database),
		bytes.NewReader([]byte("{\"sorts\":[{\"property\":\"Order\",\"direction\":\"ascending\"}]}")),
	)
	if err != nil {
		return nil, err
	}

	request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", apiKey))
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Notion-Version", "2022-02-22")

	res, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	rawResponse, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	responseData := notionResponse{}
	err = json.Unmarshal(rawResponse, &responseData)
	if err != nil {
		return nil, err
	}

	output := make([]shopHours, 0, len(responseData.Results))

	for _, r := range responseData.Results {
		title := strings.Builder{}
		for _, t := range r.Properties.Name.Title {
			title.WriteString(t.Text.Content)
		}
		output = append(output, shopHours{
			shopName: title.String(),
			dailyHours: map[uint8]openingHours{
				0: {
					openHour:    r.Properties.SundayOpenHr.Number,
					openMinute:  r.Properties.SundayOpenMin.Number,
					closeHour:   r.Properties.SundayCloseHr.Number,
					closeMinute: r.Properties.SundayCloseMin.Number,
				},
				1: {
					openHour:    r.Properties.MondayOpenHr.Number,
					openMinute:  r.Properties.MondayOpenMin.Number,
					closeHour:   r.Properties.MondayCloseHr.Number,
					closeMinute: r.Properties.MondayCloseMin.Number,
				},
				2: {
					openHour:    r.Properties.TuesdayOpenHr.Number,
					openMinute:  r.Properties.TuesdayOpenMin.Number,
					closeHour:   r.Properties.TuesdayCloseHr.Number,
					closeMinute: r.Properties.TuesdayCloseMin.Number,
				},
				3: {
					openHour:    r.Properties.WednesdayOpenHr.Number,
					openMinute:  r.Properties.WednesdayOpenMin.Number,
					closeHour:   r.Properties.WednesdayCloseHr.Number,
					closeMinute: r.Properties.WednesdayCloseMin.Number,
				},
				4: {
					openHour:    r.Properties.ThursdayOpenHr.Number,
					openMinute:  r.Properties.ThursdayOpenMin.Number,
					closeHour:   r.Properties.ThursdayCloseHr.Number,
					closeMinute: r.Properties.ThursdayCloseMin.Number,
				},
				5: {
					openHour:    r.Properties.FridayOpenHr.Number,
					openMinute:  r.Properties.FridayOpenMin.Number,
					closeHour:   r.Properties.FridayCloseHr.Number,
					closeMinute: r.Properties.FridayCloseMin.Number,
				},
				6: {
					openHour:    r.Properties.SaturdayOpenHr.Number,
					openMinute:  r.Properties.SaturdayOpenMin.Number,
					closeHour:   r.Properties.SaturdayCloseHr.Number,
					closeMinute: r.Properties.SaturdayCloseMin.Number,
				},
			},
		})
	}

	return output, nil
}
